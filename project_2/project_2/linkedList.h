/*********************************
* Class: MAGSHIMIM Final Project *
* Link struct definition	 	 *
**********************************/

#ifndef LINKEDLISTH
#define LINKEDLISTH
#define MAX_DURATION 100

#include "Frame.h"

struct Link
{
	frame_t	*frame;
	struct Link	*next;
};

typedef struct Link	link_t;
#endif

void getNewFrame(link_t ** head);
link_t * removeNode(link_t ** head,char * name,char * type);
link_t * get(void);
link_t * search(link_t * head, char * name);
void changeDuration(link_t ** head);
void changeIndex(link_t ** head);
void printList(link_t * head);
void getName(char name[]);
link_t * copy(link_t * src, link_t * dst);
void changeAllDuration(link_t ** head);
void whenPlay(link_t * head);
void freeAll(link_t ** head);
void mainOptionProject(link_t  *head);
void loadProject(link_t  **head);
void savingProject(link_t * head);
