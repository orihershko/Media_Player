#include <stdlib.h>
#include <stdio.h>
#include "linkedList.h"
#include "view.h"
#include "Frame.h"
int numberOfFrame = 0;
/*
func. that load project
INPUT:pointer to pointer of a head
OUTPUT:none
*/
void loadProject(link_t  **head)
{
	char path[MAX_PATH_SIZE] = { 0 };
	char durationStr[MAX_DURATION] = { 0 };
	int duration = 0;
	char name[MAX_NAME_SIZE] = { 0 };
	link_t * curr = NULL;
	int  i = 0;
	FILE * project = NULL;
	char c = 0;
	printf("Enter the path of project <including name>\n");
	fgets(path, MAX_PATH_SIZE, stdin);
	path[strcspn(path, "\n")] = 0;
	project = fopen(path, "r");
	while (project == NULL)
	{
		printf("The project is not exists\n			***TRY AGAIN!!***\n");
		printf("Enter the path of project <including name>\n");
		fgets(path, MAX_PATH_SIZE, stdin);
		path[strcspn(path, "\n")] = 0;
		project = fopen(path, "r");
	}
	c = fgetc(project);
	while (c != -1)
	{

		duration = 0;
		if (c == '1')
		{
			fgetc(project);// it new line
			c = fgetc(project);
			while (c != 10) // get the path
			{
				path[i] = c;
				c = fgetc(project);
				i++;
			}
			path[i] = 0;
			c = fgetc(project);
			i = 0;
			while (c != 10)// get duration
			{
				durationStr[i] = c;
				c = fgetc(project);
				i++;
			}
			durationStr[i] = 0;
			duration = atoi(durationStr);
			i = 0;
			c = fgetc(project);
			while (c != 10)
			{
				name[i] = c;
				c = fgetc(project);
				i++;
			}
			name[i] = 0;
			i = 0;
			c = fgetc(project);
			if (*head == NULL)
			{
				*head = (link_t*)malloc(sizeof(link_t));
				(*head)->frame = (frame_t*)malloc(sizeof(frame_t));
				(*head)->frame->duration = duration;
				(*head)->frame->name = (char*)malloc(sizeof(char)*strlen(name));
				(*head)->frame->path = (char*)malloc(sizeof(char)*strlen(path));
				strcpy((*head)->frame->name, name);
				strcpy((*head)->frame->path, path);
				(*head)->next = NULL;
				numberOfFrame++;
			}
			else
			{
				curr = *head;
				while (curr->next != NULL)
				{
					curr = curr->next;
				}
				curr->next = (link_t*)malloc(sizeof(link_t));
				curr->next->frame = (frame_t*)malloc(sizeof(frame_t));
				curr->next->frame->duration = duration;
				curr->next->frame->name = (char*)malloc(sizeof(char)*strlen(name));
				curr->next->frame->path = (char*)malloc(sizeof(char)*strlen(path));
				strcpy(curr->next->frame->name, name);
				strcpy(curr->next->frame->path, path);
				curr->next->next = NULL;
				numberOfFrame++;
			}
		}
	}
	fclose(project);

}

/*
func. get the option of editing project
INPUT:none
OUTPUT:none
*/
void mainOptionProject(link_t  *head)
{

	int temp = 0;
	int op = 0;
	char name[MAX_NAME_SIZE] = { 0 };
	link_t * node = NULL;

	do
	{
		printf("\n[0] exit\n[1] Add new Frame\n[2] Remove a Frame\n[3] change frame index\n[4] Change frame duration\n[5] Change duration of all frames\n[6] List frames\n[7] Play movie\n[8] save project\n");
		temp = scanf("%d", &op);
		getchar();
		if (temp != 1)
		{
			op = 9;
		}
		switch (op)
		{
		case 0:
			break;
		case 1:
			getNewFrame(&head);
			break;
		case 2:
			getName(name);
			node = removeNode(&head, name, "delete");
			if (node != NULL)
			{
				printf("The frame doesnt exists");
			}
			break;
		case 3:
			changeIndex(&head);
			break;
		case 4:
			changeDuration(&head);
			break;
		case 5:
			changeAllDuration(&head);
			break;
		case 6:
			printList(head);
			break;
		case 7:
			whenPlay(head);
			break;
		case 8:
			savingProject(head);
			break;
		default:
			printf("You should type one of the options - 0-7!\n");
			break;
		}

	} while (op != 0);
	freeAll(&head);
}


/*
func. add new frame to the list
INPUT: pointer to pointer of a head
OUTPUT:none
*/
void getNewFrame(link_t ** head)
{
	link_t * curr = *head;
	int checkName = 0;
	char  nameFrame[MAX_NAME_SIZE] = { 0 };
	link_t * newNode = NULL;
	int helpCheckName = 0;
	newNode = get();
	if (newNode != NULL)
	{
		strcpy(nameFrame, newNode->frame->name);
		while (curr != NULL && newNode != NULL)
		{
			while (strcmp(curr->frame->name, nameFrame) == 0) //please input until the name is diffrent from this
			{
				printf("The name is already taken, please enter an other name\n");
				fgets(nameFrame, MAX_NAME_SIZE, stdin);
				nameFrame[strcspn(nameFrame, "\n")] = 0;
				helpCheckName = 1;
				checkName = 1;
			}
			if (checkName == 1) // searching again to make sure that the name didnt exits
			{
				curr = *head;
				checkName = 0;

			}
			else
			{
				if (curr->next == NULL)
				{
					break;
				}
				else
				{
					curr = curr->next;
				}

			}


		}

		if (helpCheckName == 1)
		{
			newNode->frame->name = (char*)realloc(newNode->frame->name, strlen(nameFrame) + 1);
			strcpy(newNode->frame->name, nameFrame);
			newNode->frame->name[strlen(nameFrame) + 1] = 0;
		}
		if (!*head && newNode != NULL)
		{
			*head = newNode;
		}
		else if (newNode != NULL)
		{
			curr->next = newNode;
		}
		numberOfFrame++;
	}
	
}
/*
func. make new node
INPUT:none
OUTPUT: pointer of node
*/
link_t * get(void)
{
	FILE * image = 0;
	link_t * curr = NULL;
	int checkDuration = 0;
	char path[MAX_PATH_SIZE] = { 0 };
	char name[MAX_NAME_SIZE] = { 0 };
	curr = (link_t*)malloc(sizeof(link_t));
	curr->frame = (frame_t*)malloc(sizeof(frame_t));
	printf("                *** Creating new frame ***\nPlease insert frame path:\n");
	fgets(path, MAX_PATH_SIZE, stdin);
	path[strcspn(path, "\n")] = 0;
	image = fopen(path, "rb");
	if (image == NULL)
	{
		printf("Can't find file! Frame will not be added");
		return NULL;
	}
	else
	{
		fclose(image);
		printf("Please insert frame duration(in miliseconds):\n");
		checkDuration = scanf("%d", &curr->frame->duration);
		getchar();
		if (checkDuration == 1)
		{
		getName(name);
			curr->next = NULL;

			curr->frame->name = (char*)malloc(strlen(name)+1);
			strcpy(curr->frame->name, name);
			curr->frame->name[strlen(name) + 1] = 0;
			curr->frame->path = (char*)malloc(strlen(path)+1);
			strcpy(curr->frame->path, path);
			curr->frame->path[strlen(path) + 1] = 0;
			return curr;
		}
		else
		{
			return NULL;
		}
	}
}
/*
func. get the name of the frame
INPUT:none
OUTPUT: string
*/
void getName(char name[])
{
	
	printf("Enter name of frame\n");
	fgets(name, MAX_NAME_SIZE, stdin);
	name[strcspn(name, "\n")] = 0;

}
/*
func. remove and node from the list
INPUT:pointer to pointer of  a head  , name of frame , type of using
OUTPUT:NULL for delete  the node or the node out from the list
*/
link_t * removeNode(link_t ** head, char * name, char * type)
{
	link_t * curr = *head;
	link_t * temp = NULL;
	link_t * nextNode = NULL;
	if (head != NULL)
	{
		if (strcmp((*head)->frame->name, name) == 0)
		{
			*head = curr->next;
			if (strcmp(type, "change") == 0)
			{
				temp = copy(temp, curr);
			}
			free(curr);
		}
		else
		{
			while (curr->next != NULL)
			{
				if (strcmp(curr->next->frame->name, name) == 0)// find the previous node
				{
					break;
				}
				curr = curr->next;
			}

			if (curr != NULL || curr->next != NULL)
			{
				nextNode = curr->next->next;
				if (strcmp(type, "change") == 0)
				{
					temp = copy(temp, curr->next);

				}
				free(curr->next);
				curr->next = nextNode;
			}

		}

	}
	return temp;

}/*
 func.copy node to othe node and free the dst
 INPUT:src and dst
 OUTPUT:pointer of node
 */
link_t * copy(link_t * src, link_t * dst)
{
	src = (link_t*)malloc(sizeof(link_t));
	src->frame = (frame_t*)malloc(sizeof(frame_t));
	src->frame->duration = dst->frame->duration;
	src->frame->name = (char*)malloc(sizeof(dst->frame->name));
	src->frame->path = (char*)malloc(sizeof(dst->frame->path));
	if (src->frame->name == NULL || src->frame->path == NULL)
	{
		printf("Malloc error\n");
		return NULL;
	}
	strcpy(src->frame->name, dst->frame->name);
	strcpy(src->frame->path, dst->frame->path);
	return src;
}
/*
func. change the index of node
INPUT:pointer to pointer of  a head
OUTPUT:none
*/
void changeIndex(link_t ** head)
{
	link_t * node1 = NULL;
	link_t * temp = NULL;
	link_t * curr = *head;
	int index = 0;
	int i = 0;
	char frameName1[MAX_NAME_SIZE] = { 0 };
	getName(frameName1);
	node1 = search(*head, frameName1);
	if (node1 == NULL)
	{
		printf("One of the frames dont exists");
	}
	else
	{
		printf("Enter the new index in the movie you wish to place the frame\n");
		scanf("%d", &index);
		getchar();
		while (index> numberOfFrame)
		{
			printf("The movie contains only %d frames!\n", numberOfFrame);
			scanf("%d", &index);
			getchar();
		}
		if (i != index && index != 1)
		{
			for (i = 0; i < index - 1; ++i)
			{
				curr = curr->next;
			}

			temp = removeNode(head, frameName1, "change");
			temp->next = curr->next;
			curr->next = temp;
		}
		else
		{
			temp = removeNode(head, frameName1, "change");
			temp->next = (*head);
			*head = temp;
		}

	}
}
/*
func. search if the frame exists in the list
INPUT:pointer to head and the name to search
OUTPUT:node for exists of NULL for not
*/
link_t * search(link_t * head, char  * name)
{
	link_t * curr = head;


	while (curr != NULL)
	{
		if (strcmp(curr->frame->name, name) == 0)
		{
			break;
		}
		curr = curr->next;
	}
	return curr;
}
/*
func.change duration of one node
INPUT:pointer to pointer of a head
OUTPUT:none
*/
void changeDuration(link_t ** head)
{
	link_t * curr = NULL;
	int temp = 0;
	int duration = 0;
	char framName[MAX_NAME_SIZE] = { 0 };
	getName(framName);
	curr = search(*head, framName);
	if (curr == NULL)
	{
		printf("The frame doesnt exists");
	}
	else
	{
		printf("Enter new duration\n");
		temp = scanf("%d", &duration);
		while (temp != 1 || duration < 0)
		{
			printf("The duration doesnt currect try again\n");
			temp = scanf("%d", &duration);
		}
		curr->frame->duration = duration;

	}
}
/*
func. change all th eduration of the nods
INPUT:pointer to pointer of a head
OUTPUT: none
*/
void changeAllDuration(link_t ** head)
{
	int unsigned duration = 0;
	int temp = 0;
	link_t * curr = *head;
	printf("Enter the duration for all frames\n");
	temp = scanf("%d", &duration);
	while (temp != 1)
	{
		printf("The duration doesnt currect try again\n");
		temp = scanf("%d", &duration);
	}
	while (curr != NULL)
	{
		curr->frame->duration = duration;
		curr = curr->next;
	}

}

void savingProject(link_t * head)
{
	char path[MAX_PATH_SIZE] = { 0 };
	FILE * project = NULL;
	link_t * curr = head;
	printf("Enter the path that the project will save\n");
	fgets(path, MAX_PATH_SIZE, stdin);
	path[strcspn(path, "\n")] = 0;
	project = fopen(path, "w");
	while (curr != NULL)
	{
		fprintf(project, "1\n%s\n%d\n%s\n", curr->frame->path, curr->frame->duration, curr->frame->name);
		curr = curr->next;
	}
	fclose(project);
	printf("The project %s", path);
}
/*
func. that play the frame
INPUT:pointer of a head
OUTPUT:none
*/
void whenPlay(link_t * head)
{
	int unsigned op = 0;
	int temp = 0;
	link_t * curr = NULL;
	char name[MAX_NAME_SIZE] = { 0 };
	printf("If you wish to play the whole movie press 1\n");
	printf("If you want to play the movie from a certain point press 2\n");
	temp = scanf("%d", &op);
	getchar();
	while (temp != 1 && (op != 1 || op != 2))
	{
		printf("The option doesnt currect try again\n");
		temp = scanf("%d", &op);
		getchar();
	}
	if (op == 1)
	{
		play(head);
	}
	else if (op == 2)
	{
		printf("Where do you wish to start playing the movie from?\n");
		fgets(name, MAX_NAME_SIZE, stdin);
		name[strcspn(name, "\n")] = 0;
		curr = search(head, name);
		while (curr == NULL)
		{
			printf("The frame doesnt exists try again\n");
			fgets(name, MAX_NAME_SIZE, stdin);
			name[strcspn(name, "\n")] = 0;
			curr = search(head, name);
		}
		play(curr);
	}
}
/*
func. print the list
INPUT:pointer of a head
OUTPUT:none
*/
void printList(link_t * head)
{
	link_t * curr = head;
	printf("	Name		Duration	Path\n");
	while (curr != NULL)
	{
		printf("    %s    %d ms    %s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
}


void freeAll(link_t ** head)
{
	link_t *  temp = NULL;
	link_t * curr = *head;
	while (curr != NULL)
	{
		temp = curr;
		curr = temp->next;
		free(temp);
	}


}